### Tutor 08

> Add Bootstrap CSS Framework

* Add Bootstrap CSS

* Standard Header (jquery or vue or native) and Footer

* Enhance All Layouts with Bootstrap CSS

* Apply Bootstrap Two Column Responsive Layout for Most Layout

![Jekyll Bootstrap: Tutor 08][jekyll-bootstrap-preview-08]

-- -- --

What do you think ?

[jekyll-bootstrap-preview-08]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-08/jekyll-bootstrap-oc-preview.png
