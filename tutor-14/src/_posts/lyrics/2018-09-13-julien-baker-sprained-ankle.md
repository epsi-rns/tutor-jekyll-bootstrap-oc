---
layout      : post
title       : Julien Baker - Sprained Ankle
date        : 2018-09-13 07:35:05 +0700
categories  : lyric
tags        : [rock, 2010s]
keywords    : [sad, emo, broken]

author      : Julien Baker
toc         : toc/2018-09-julien-baker.html

related_link_ids :
  - 18090735  # Something

excerpt: Wish I could write songs about anything other than death
---

A sprinter learning to wait  
A marathon runner, my ankles are sprained  
A marathon runner, my ankles are sprained
