---
layout      : post
title       : Mothers -  No Crying in Baseball
date        : 2019-07-15 07:35:05 +0700
categories  : lyric
tags        : [indie, 2010s]
author      : Mothers
---

There's no crying in baseball.  
Try to understand.

# Heading 1

## Heading 2

### Example Content

[Terminal Dofiles][one-link].

* are you not learning
* have you not been listening
* are you not learning
* you have not been listening

![Business Card][one-image]

-- -- --

### Example Dotfiles

ViM RC: [bandithijo][link-dotfiles]

-- -- --

### What's next ?

Our next song would be [Company of Thieves - Oscar Wilde][local-whats-next]

[//]: <> ( -- -- -- links below -- -- -- )

{% assign asset_path = '/assets/images' %}
{% assign dotfiles = 'https://gitlab.com/epsi-rns/dotfiles/tree/master' %}

[local-whats-next]: /lyric/2020/03/15/company-of-thieves-oscar-wilde.html
[link-dotfiles]:    {{ dotfiles }}/terminal/vimrc/vimrc.bandithijo

[one-image]:    {{ asset_path }}/cards/one-page.png
[one-link]:     {{ dotfiles }}/terminal/
