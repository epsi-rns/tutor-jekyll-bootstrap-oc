### Tutor 14

> Jekyll Theme with Assets and SASS

* Bundling Gem

* Using Theme

![Jekyll Bootstrap: Tutor 14][jekyll-bootstrap-preview-14]

-- -- --

What do you think ?

[jekyll-bootstrap-preview-14]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-14/jekyll-bootstrap-oc-preview.png
