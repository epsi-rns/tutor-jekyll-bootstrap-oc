// https://mpetroff.net/2015/03/bootstrap-navbar-without-jquery/

document.addEventListener("DOMContentLoaded", function(event) { 
  // Check for click events on the navbar burger icon
  var toggler = document.getElementsByClassName('navbar-toggler')[0];
  var collapse = document.getElementById('navbarCollapse');
  var dropdownButton = document.getElementById('navbarDropdown');
  var dropdownMenu   = document.querySelector('[aria-labelledby="navbarDropdown"]');
  
  // Toggle if navbar menu is open or closed
  function toggleMenu() {
    collapse.classList.toggle('show');
    console.log('Toggle show class in navbar burger menu.');
  }

  function toggleDropdown() {
    dropdownMenu.classList.toggle('show');
    console.log('Toggle show class in dropdown submenu.');
  }
  
  // Close dropdowns when screen becomes big enough to switch to open by hover
  function closeMenusOnResize() {    
    if (collapse.classList.contains('show')) {
        console.log('Remove show class before collapsing.');
        dropdownMenu.classList.remove('show');
    }
  }

  // Event listeners
  window.addEventListener('resize', closeMenusOnResize, false);
  toggler.addEventListener('click', toggleMenu, false);
  dropdownButton.addEventListener('click', toggleDropdown, false);
});
