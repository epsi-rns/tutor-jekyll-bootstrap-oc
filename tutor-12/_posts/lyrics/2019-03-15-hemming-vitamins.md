---
layout      : post
title       : Hemming - Vitamins
date        : 2019-03-15 07:35:05 +0700
categories  : lyric
tags        : [rock, 2010s]
author      : Hemming

opengraph:
  image: https://t2.genius.com/unsafe/220x220/https%3A%2F%2Fimages.genius.com%2F8d746692adf5ce3495cab0c9fe2508cb.600x600x1.jpg
---

You swallow me whole without even thinking now  
Your hands are as cold as whatever you're drinking down

{% include shortcodes/advert.html img="oto-spies-01.png" %}

Been trying to fill all the holes you've been digging for yourself  
But I can't replace everything that's gone missing from your shell

{% include shortcodes/advert.html img="oto-spies-02.png" %}

Do you think I'll make you feel better?  
Do you think I'll make you feel better?
