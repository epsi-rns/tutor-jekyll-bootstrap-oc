---
layout      : post
title       : Emily King - Distance
date        : 2018-01-15 07:35:05 +0700
categories  : lyric
tags        : [jazz, 2010s]
author      : Emily King

opengraph:
  image: https://t2.genius.com/unsafe/220x220/https%3A%2F%2Fimages.rapgenius.com%2Fdd05d57af558575db3dda4663692ae52.700x700x1.jpg
---

It makes the leaves on the trees fall  
It makes the hours in the day long  
Makes me wanna clear my head  
Find a little cafe and write the words to a song

When we are apart  
Distance makes the heart grow  
Even when I’m lonely  
Happy knowing that your love is never far

In the light or in the dark  
On a plane up in the stars  
In a movie or at the bar  
Home alone or out to tea

We get together we make it good


### Pesan Tersemat

ATURAN GRUP BOOTSTRAP INDONESIA TELEGRAM

`#ATURAN` `#RULE` `#RULES`

1. Bagi member baru dipersilahkan untuk **memperkenalkan diri** 🤝.

2. 🚫 Dilarang posting 👉: `spam`, `sara`, `pornografi`, sesuatu yg tidak ada hubungannya dengan `teknologi`, `pemrograman` dan mengganggu suasana `kondusifitas` group.

3. ❌ Jangan bertanya untuk bertanya. Contohnya `"Boleh nanya gak gan?"`. Langsung saja bertanya dengan **jelas** dengan disertai **tanda tanya** (?)

4. Apabila hendak bertanya, diawali dengan **hastag** `#ASK` lalu kategori pertanyaan -> `#ASK #CI #auth_login`

5. Apabila posting **loker/project**, sertakan `deskripsi pekerjaan, benefit/salary, tempat, waktu` dan `kontak yg bisa dihubungi`.

6. Jenis loker/project ✅ seputar teknologi bidang **IT** dan pemrograman  👉 : `system admin`, `networking`, `programmer` (web, desktop dan mobile), `design grafis`, dan sejenisnya.

7. 🚫 Dilarang posting loker dimana **informasi pekerjaannya lewat japri** (supaya menghindari penipuan).

8. Gunakan fitur **Reply** untuk memudahkan berkomunikasi (jawab pertanyaan, silaturahmi, dll) antar member.

9. Apabila hendak berbagi tutorial/modul/info dan sejenisnya, sertakan **sumber** yang valid jika tersedia dan hastag `#modul` atau `#tutorial` atau `#info` (sesuaikan) diikuti jenis tutorial -> `#modul` `#library` atau `#tutorial` `#javascript`, atau [`#html5`, `#css3`, `#sass` `#grunt` `#npm` `#gem` `#pwa` `#ssg`].

10. Gunakan **tanda baca** dan **bahasa** dengan ejaan yg baik dan benar (Indonesia, Inggris atau bahasa daerah) agar mudah dipahami oleh member lain serta tidak memicu ini kesalahpahaman.

11. Biasakan menggunakan code paste tool (`pastebin.com`, `justpaste.it`, `gist`, `codepen` atau yang lain) dan screnshoot (bilamana memungkinkan) sebagai penunjang pertanyaan.

12. Panggil member dengan panggilan `paman`, `om`, `mas`, `pak`, `mba`, `ibu`, selain itu ❌ dilarang

#### ☆ Bahan Bacaan

* 🕷 <https://epsi-rns.gitlab.io/frontend/2019/02/15/concept-css/>

