---
layout      : post
title       : Brooke Annibale - By Your Side
date        : 2019-05-15 07:35:05 +0700
categories  : lyric
tags        : [pop, 2010s]
keywords    : [OST, "Grey's Anatomy"]

author      : Brooke Annibale
toc         : toc/2019-15-brooke-annibale.html

related_link_ids :
  - 19052535  # Yours and Mine

excerpt     :
  No more airplanes.
  No more long goodbyes.

opengraph:
  image: https://t2.genius.com/unsafe/220x220/https%3A%2F%2Fimages.genius.com%2Fb60b95ddf3c9116dc25191eaa93f8931.1000x1000x1.jpg
---

Someday some day soon

```haskell
-- Layout Hook
commonLayout = renamed [Replace "common"]
    $ avoidStruts 
    $ gaps [(U,5), (D,5)] 
    $ spacing 10
    $ Tall 1 (5/100) (1/3)
```

No more airplanes  
No more long goodbyes  
'Cause I'm staying right by your side
