### Tutor 12

> Finishing

* Layout: Service (dummy)

* Post: Markdown Content (test case)

* Post: Table of Content (dynamic include)

* Post: Responsive Images

* Post: Syntax Highlight (sass)

* Post: Shortcodes (include with parameter)

* Official Plugin: Feed

* Meta: HTML, SEO, Opengraph, Twitter

* Multi Column Responsive List: Categories, Tags, and Archives

![Jekyll Bootstrap: Tutor 12][jekyll-bootstrap-preview-12]

-- -- --

What do you think ?

[jekyll-bootstrap-preview-12]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-12/jekyll-bootstrap-oc-preview.png
