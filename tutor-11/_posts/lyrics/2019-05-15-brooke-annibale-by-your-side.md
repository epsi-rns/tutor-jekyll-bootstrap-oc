---
layout      : post
title       : Brooke Annibale - By Your Side
date        : 2019-05-15 07:35:05 +0700
categories  : lyric
tags        : [pop, 2010s]
keywords    : [OST, "Grey's Anatomy"]

author      : Brooke Annibale
toc         : toc/2019-15-brooke-annibale.html

related_link_ids :
  - 19052535  # Yours and Mine
---

Someday some day soon

No more airplanes  
No more long goodbyes  
'Cause I'm staying right by your side
