# Jekyll Bootstrap OC Test Drive

An example of Jekyll site using Bootstrap Open Color
for personal learning purpose.

> Jekyll (Liquid) + Bootstrap (Open Color)

![Jekyll Plain: Tutor][jekyll-bootstrap-preview]

-- -- --

## Jekyll Version

Since this repository is still using Jekyll 3.8, instead of Jekyll 4.0,
you might need to run `bundle install`, depend on the situation.

	bundle install --full-index

-- -- --

## Jekyll Step By Step

This Tutorial is made in two stages.

1. Jekyll in plain HTML:
   covering liquid, avoiding burden of any stylesheet

2. Jekyll with CSS Frameworks:
   with two offered choices, either Bulma or Bootstrap.
   
### Stage 1: Plain

* [Jekyll Step by Step Repository][tutorial-jekyll-plain]

Contain Tutor-01 until Tutor-05.
With additional Tutor-06 to create plugins,
and Tutor-07 to create basic theme in Gem.

### Stage 2: Bulma MD

> This repository:

* [Jekyll (Bulma MD)][tutorial-jekyll-bmd]

Each contain Tutor-08 until Tutor-12.
With additional Tutor-13
as a complete Bulma site with stylesheet and plugins,
and Tutor-14 as complete theme in Gem.

### Stage 2: Bootstrap OC

Alternative implementation choice.

* [Jekyll (Bootstrap OC)][tutorial-jekyll-boc]

Each contain Tutor-08 until Tutor-12.
With additional Tutor-13 and Tutor-14.

### Jekyll Tutorial

A thorough Jekyll tutorial can be found in this article series:

* [Jekyll - Overview][series-jekyll]

[tutorial-jekyll-bmd]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/
[tutorial-jekyll-boc]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/
[tutorial-jekyll-plain]:https://gitlab.com/epsi-rns/tutor-jekyll-plain/
[series-jekyll]:        https://epsi-rns.gitlab.io/ssg/2020/06/01/jekyll-overview/

-- -- --

## Chapter Step by Step

### Tutor 08

> Add Bootstrap CSS Framework

* Add Bootstrap CSS

* Standard Header (jquery or vue or native) and Footer

* Enhance All Layouts with Bootstrap CSS

* Apply Bootstrap Two Column Responsive Layout for Most Layout

![Jekyll Bootstrap: Tutor 08][jekyll-bootstrap-preview-08]

-- -- --

### Tutor 09

> Combine with Custom SASS Material Design

* Add (a bunch of) Custom SASS (Custom Design)

* Nice Header and Footer

* Enhance All Two Column Responsive Layout with Material Design

* Nice Tag Badge in Tags Page

![Jekyll Bootstrap: Tutor 09][jekyll-bootstrap-preview-09]

-- -- --

### Tutor 10

> Loop with Liquid

* More Content: Quotes. Need this content for demo

* Simplified All Layout Using Liquid Template Inheritance

* Article Index: By Year, List Tree (By Year and Month)

![Jekyll Bootstrap: Tutor 10][jekyll-bootstrap-preview-10]

-- -- --

### Tutor 11

> Features

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

* Refactoring Template using Capture: Widget

* Pagination (v1): Adjacent, Indicator, Responsive

* Pagination (v2): Adjacent, Indicator, Responsive

* Post: Header, Footer, Navigation

![Jekyll Bootstrap: Tutor 11][jekyll-bootstrap-preview-11]

-- -- --

### Tutor 12

> Finishing

* Layout: Service (dummy)

* Post: Markdown Content (test case)

* Post: Table of Content (dynamic include)

* Post: Responsive Images

* Post: Syntax Highlight (sass)

* Post: Shortcodes (include with parameter)

* Official Plugin: Feed

* Meta: HTML, SEO, Opengraph, Twitter

* Multi Column Responsive List: Categories, Tags, and Archives

![Jekyll Bootstrap: Tutor 12][jekyll-bootstrap-preview-12]

-- -- --

### Tutor 13

> Jekyll Plugin with Ruby

* Filter: Year Text, Term Array, Pagination Links, Link Offset Flag, Keywords

* Generator: Tag Names

![Jekyll Bootstrap: Tutor 13][jekyll-bootstrap-preview-13]

-- -- --

### Tutor 14

> Jekyll Theme with Assets and SASS

* Bundling Gem

* Using Theme

![Jekyll Bootstrap: Tutor 14][jekyll-bootstrap-preview-14]

-- -- --

## Additional Links

### HTML Step By Step

Additional guidance:

* [Bulma Material Design Step by Step Repository][tutorial-bulma-md] (frankenbulma)

* [Bulma Step by Step Repository][tutorial-bulma]

* [Materialize Step by Step Repository][tutorial-materialize]

### Comparison

Comparation with other static site generator

* [Eleventy (Materialize) Step by Step Repository][tutorial-11ty-m]

* [Eleventy (Bulma MD) Step by Step Repository][tutorial-11ty-b]

* [Hugo Step by Step Repository][tutorial-hugo]

* [Hexo Step by Step Repository][tutorial-hexo]

* [Pelican Step by Step Repository][tutorial-pelican]

### Presentation

* [Concept SSG - Presentation Slide][ssg-presentation]

* [Concept CSS - Presentation Slide][css-presentation]

[tutorial-pelican]:     https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/
[tutorial-hugo]:        https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/
[tutorial-11ty-m]:      https://gitlab.com/epsi-rns/tutor-11ty-materialize/
[tutorial-11ty-b]:      https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/
[tutorial-hexo]:        https://gitlab.com/epsi-rns/tutor-hexo-bulma/

[tutorial-bulma-md]:    https://gitlab.com/epsi-rns/tutor-html-bulma-md/
[tutorial-bulma]:       https://gitlab.com/epsi-rns/tutor-html-bulma/
[tutorial-materialize]: https://gitlab.com/epsi-rns/tutor-html-materialize/

[ssg-presentation]:     https://epsi-rns.gitlab.io/ssg/2019/02/17/concept-ssg/
[css-presentation]:     https://epsi-rns.gitlab.io/frontend/2019/02/15/concept-css/

-- -- --

What do you think ?

[jekyll-bootstrap-preview]:     https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/jekyll-bootstrap-oc-preview.png
[jekyll-bootstrap-preview-08]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-08/jekyll-bootstrap-oc-preview.png
[jekyll-bootstrap-preview-09]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-09/jekyll-bootstrap-oc-preview.png
[jekyll-bootstrap-preview-10]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-10/jekyll-bootstrap-oc-preview.png
[jekyll-bootstrap-preview-11]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-11/jekyll-bootstrap-oc-preview.png
[jekyll-bootstrap-preview-12]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-12/jekyll-bootstrap-oc-preview.png
[jekyll-bootstrap-preview-13]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-13/jekyll-bootstrap-oc-preview.png
[jekyll-bootstrap-preview-14]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-14/jekyll-bootstrap-oc-preview.png
