### Tutor 10

> Loop with Liquid

* More Content: Quotes. Need this content for demo

* Simplified All Layout Using Liquid Template Inheritance

* Article Index: By Year, List Tree (By Year and Month)

![Jekyll Bootstrap: Tutor 10][jekyll-bootstrap-preview-10]

-- -- --

What do you think ?

[jekyll-bootstrap-preview-10]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-10/jekyll-bootstrap-oc-preview.png
