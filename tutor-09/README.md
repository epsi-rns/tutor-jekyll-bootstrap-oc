### Tutor 09

> Combine with Custom SASS Material Design

* Add (a bunch of) Custom SASS (Custom Design)

* Nice Header and Footer

* Enhance All Two Column Responsive Layout with Material Design

* Nice Tag Badge in Tags Page

![Jekyll Bootstrap: Tutor 09][jekyll-bootstrap-preview-09]

-- -- --

What do you think ?

[jekyll-bootstrap-preview-09]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-09/jekyll-bootstrap-oc-preview.png
