module Jekyll
  module PaginationLinks
    def pagination_links(paginator, baseurl, paginate_root, permalink)
      _total     = paginator['total_pages']
      _first     = baseurl + paginate_root
      _permalink = ->(i) { (i==1) ? "" : permalink.sub(":num", i.to_s) }

      {
        "prev"  => baseurl + ( paginator['previous_page_path'] || "" ),
        "next"  => baseurl + ( paginator['next_page_path'] || "" ),
        "first" => _first,
        "last"  => _first + _permalink.(_total),
        "pages" => Hash[(1.._total).collect { # key pairs
          |i| [i, _first + _permalink.(i)] 
        }]
      }
    end
  end
end

Liquid::Template.register_filter(Jekyll::PaginationLinks)

=begin
    def pagination_links(paginator, baseurl, paginate_root, permalink)

      ppp = paginator['previous_page_path']
      npp = paginator['next_page_path']
      tp  = paginator['total_pages']
      
      _first = baseurl + paginate_root
      _other = baseurl + paginate_root + permalink
      
      pages = Hash.new

      (1..tp).each do |i|
        pages[i] = (i==1) ? _first : _other.sub(":num", i.to_s)
      end

      Hash[
        "prev"  => baseurl + (ppp ? ppp : ""),
        "next"  => baseurl + (npp ? npp : ""),
        "first" => _first,
        "last"  => _other.sub(":num", tp.to_s),
        "pages" => pages
      ]
    end
=end

=begin
      pages = Hash[(1..tp).collect { # key pairs
        |i| [i, (i==1) ? _first : _other.sub(":num", i.to_s)] 
      }]
=end
