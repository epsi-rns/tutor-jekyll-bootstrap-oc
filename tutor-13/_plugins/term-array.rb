module Jekyll
  module TermArray
    def term_array(terms)
      terms.keys
    end
  end
end

Liquid::Template.register_filter(Jekyll::TermArray)

=begin
    def term_array_alternate_one(terms)
      terms.map { |term| term.at(0) }
    end

    def term_array_alternate_two(terms)
      a = []
      terms.each { |term| a << term.at(0) }
      a
    end
=end
