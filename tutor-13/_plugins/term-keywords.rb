module Jekyll
  module TermKeywords
    def term_keywords(cats, tags, keywords)
      [cats, tags, keywords].reject{ |array| array.nil? }.flatten
    end
  end
end

Liquid::Template.register_filter(Jekyll::TermKeywords)

=begin
    def term_keywords(cats, tags, keywords)
      terms = []
      terms += cats     if !cats.nil?     && !cats.empty?
      terms += tags     if !tags.nil?     && !tags.empty?
      terms += keywords if !keywords.nil? && !keywords.empty?

      terms
    end
=end
