---
layout      : post
title       : Nicole Atkins - A Night of Serious Drinking
date        : 2017-03-25 07:35:05 +0700
categories  : lyric
tags        : [soul, 2010s]

author      : Nicole Atkins
toc         : toc/2017-03-nicole-atkins.html

related_link_ids :
  - 17031535  # Crazy

excerpt     :
  This jet plane lands silently still.
  Can’t believe I’ll be there soon.
  Three years ago felt like a lifetime.

mathjax     : true
---

On a jet plane sailing through the night  
I find I’m thinking of you  
9 years ago was just like yesterday

$$ \lim\limits_{x \to \infty} \exp(-x) = 0 $$

On a jet plane sailing through the night  
I guess I’m wasting precious time  
Seven years ago you told me you had plans to go

$$ \int_0^\infty \mathrm{e}^{-x}\,\mathrm{d}x $$

This jet plane lands silently still  
Can’t believe I’ll be there soon  
Three years ago felt like a lifetime
