---
layout      : post
title       : Brooke Annibale - Yours and Mine
date        : 2019-05-25 07:35:05 +0700
categories  : lyric
tags        : [pop, 2010s]
keywords    : [OST, "One Tree Hill"]

author      : Brooke Annibale
toc         : toc/2019-15-brooke-annibale.html

related_link_ids :
  - 19051535  # By Your Side

excerpt     : >-
  Take yours and mine, we'll combine.
  And we'll have everything.
---

All we've got is all we have to give away  
So what I have not I fill in with what you got  
And we'll be okay

{% highlight haskell %}
-- Layout Hook
commonLayout = renamed [Replace "common"]
    $ avoidStruts 
    $ gaps [(U,5), (D,5)] 
    $ spacing 10
    $ Tall 1 (5/100) (1/3)
{% endhighlight %}

Take yours and mine, we'll combine  
And we'll have everything

Take yours and mine, we'll combine  
And we'll have everything
