### Tutor 13

> Jekyll Plugin with Ruby

* Filter: Year Text, Term Array, Pagination Links, Link Offset Flag, Keywords

* Generator: Tag Names

![Jekyll Bootstrap: Tutor 13][jekyll-bootstrap-preview-13]

-- -- --

What do you think ?

[jekyll-bootstrap-preview-13]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-13/jekyll-bootstrap-oc-preview.png
